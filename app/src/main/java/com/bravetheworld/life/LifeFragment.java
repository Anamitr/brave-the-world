package com.bravetheworld.life;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.bravetheworld.MainActivity;
import com.bravetheworld.R;
import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.BaseFragment;
import com.bravetheworld.general.model.Match;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Konrad Sochacki on 09.06.2018.
 */
public class LifeFragment extends BaseFragment {
    private static final String TAG = LifeFragment.class.getSimpleName();

    private ListView matchesListView;
    private ProgressBar mProgressBar;

    private DatabaseReference mMatchesForUsersDatabaseReference;
    private DatabaseReference mMatchesDatabaseReference;

    private MatchesAdapter mMatchesAdapter;

    private List<Match> mMatchesList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_life, container, false);
        matchesListView = view.findViewById(R.id.matches_list_view);
        mProgressBar = view.findViewById(R.id.progressBar);
        mProgressBar.setVisibility(ProgressBar.VISIBLE);

        mMatchesList = new ArrayList<>();

        initDatabaseReferences();

        if(isUserSignedIn()) {
            initMatchesAdapter();
        }

        return view;
    }

    private void initDatabaseReferences() {
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mMatchesForUsersDatabaseReference = mFirebaseDatabase.getReference().child(getContext().getString(R.string.table_matches_for_users));
        mMatchesDatabaseReference = mFirebaseDatabase.getReference().child(getContext().getString(R.string.table_matches));
    }

    private void initMatchesAdapter() {
        mMatchesAdapter = new MatchesAdapter(getContext(), R.layout.list_item_user, mMatchesList);
        matchesListView.setAdapter(mMatchesAdapter);
        setUpMatchesForUsersListener();
    }

    private void setUpMatchesForUsersListener() {
        mMatchesForUsersDatabaseReference.child(FirebaseAuth.getInstance().getUid()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.v(TAG, "datasnapshot key: " + dataSnapshot.getKey());
                String matchForUser = dataSnapshot.getKey();
                mMatchesDatabaseReference.child(matchForUser).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Match match = dataSnapshot.getValue(Match.class);
                        mMatchesAdapter.add(match);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        mMatchesForUsersDatabaseReference.child(FirebaseAuth.getInstance().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.v(TAG, "Matches for user loaded");
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
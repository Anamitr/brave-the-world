package com.bravetheworld.life.chat;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.bravetheworld.R;
import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.BaseActivity;
import com.bravetheworld.general.model.Message;
import com.bravetheworld.general.model.User;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Konrad Sochacki on 10.06.2018.
 */
public class ChatActivity extends BaseActivity {
    private static final String TAG = ChatActivity.class.getSimpleName();

    public static final int DEFAULT_MSG_LENGTH_LIMIT = 1000;
    public static final String CHAT_ID_KEY = "CHAT_ID_KEY";
    public static final String CHATTING_PARTNER_KEY = "CHATTING_PARTNER_KEY";
    public static final String CHATTING_PARTNER_PROFILE_PICTURE_KEY = "CHATTING_PARTNER_PROFILE_PICTURE_KEY";

    private Toolbar mChatToolbar;
    private RecyclerView mMessageRecyclerView;
    private ProgressBar mProgressBar;
    private EditText mMessageEditText;
    private ImageButton mSendButton;

    private MessageRecyclerViewAdapter mMessageRecyclerViewAdapter;
    private List<Message> mMessageList;

    private String mChatId;
    private User mChattingPartner;
    private Bitmap mChattingPartnerProfilePicture;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mMessagesDatabaseReference;
    private ChildEventListener mChildEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mFirebaseDatabase = FirebaseDatabase.getInstance();

        mChatId = getIntent().getStringExtra(CHAT_ID_KEY);
        mChattingPartner = (User) getIntent().getSerializableExtra(CHATTING_PARTNER_KEY);
        mChattingPartner = (User) getIntent().getSerializableExtra(CHATTING_PARTNER_KEY);
        mChattingPartnerProfilePicture = getIntent().getParcelableExtra(CHATTING_PARTNER_PROFILE_PICTURE_KEY);

        mMessagesDatabaseReference = mFirebaseDatabase.getReference().child(getString(R.string.table_chats)).child(mChatId);

        // Initialize references to views
        mChatToolbar = findViewById(R.id.chat_toolbar);
        mMessageRecyclerView = findViewById(R.id.reyclerview_message_list);
        mProgressBar = findViewById(R.id.progress_bar);
        mMessageEditText = findViewById(R.id.message_edit_text);
        mSendButton = findViewById(R.id.send_button);

        setUpChatToolbar();

        mMessageList = new ArrayList<>();

        attachDatabaseReadListener();

        // Initialize message RecyclerView and its adapter
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setStackFromEnd(true);
        mMessageRecyclerView.setLayoutManager(linearLayoutManager);
        mMessageRecyclerViewAdapter = new MessageRecyclerViewAdapter((ApplicationContext) getApplicationContext(), mMessageList, mChattingPartnerProfilePicture);
        mMessageRecyclerView.setAdapter(mMessageRecyclerViewAdapter);

        // Initialize progress bar
        mProgressBar.setVisibility(ProgressBar.VISIBLE);

        // Enable Send button when there's text to send
        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    mSendButton.setEnabled(true);
                } else {
                    mSendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        mMessageEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(DEFAULT_MSG_LENGTH_LIMIT)});

        // Send button sends a message and clears the EditText
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message message = new Message(mMessageEditText.getText().toString(), ((ApplicationContext) getApplicationContext()).getAuthenticatedUser().id);
                mMessagesDatabaseReference.push().setValue(message);

                // Clear input box
                mMessageEditText.setText("");
            }
        });
    }

    private void setUpChatToolbar() {
        setSupportActionBar(mChatToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(mChattingPartner.getNick());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void attachDatabaseReadListener() {
        mChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Message message = dataSnapshot.getValue(Message.class);
                mMessageList.add(message);
                mMessageRecyclerView.scrollToPosition(mMessageList.size() - 1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        mMessagesDatabaseReference.addChildEventListener(mChildEventListener);

        // Check if messages loading has finished
        mMessagesDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.v(TAG, "Messages loaded");
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        attachDatabaseReadListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //attachDatabaseReadListener();
    }

    private void detachDatabaseReadListener() {
        if (mChildEventListener != null) {
            mMessagesDatabaseReference.removeEventListener(mChildEventListener);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        detachDatabaseReadListener();
        super.onStop();
    }
}

package com.bravetheworld.life.chat;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bravetheworld.R;
import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.model.Message;

import java.util.List;

public class MessageRecyclerViewAdapter extends RecyclerView.Adapter {
    private static final String TAG = MessageRecyclerViewAdapter.class.getSimpleName();

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private ApplicationContext mApplicationContext;
    private List<Message> mMessageList;
    private Bitmap chattingPartnerProfilePicture;

    public MessageRecyclerViewAdapter(ApplicationContext applicationContext, List<Message> mMessageList, Bitmap chattingPartnerProfilePicture) {
        this.mApplicationContext = applicationContext;
        this.mMessageList = mMessageList;
        this.chattingPartnerProfilePicture = chattingPartnerProfilePicture;
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Message message = mMessageList.get(position);

        if (message.getUserId().equals(((mApplicationContext)).getAuthenticatedUser().getId())) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Message message = mMessageList.get(position);
        holder.setIsRecyclable(false);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                if(isLastInSequence(position, message)) {
                    ((ReceivedMessageHolder) holder).bind(message, chattingPartnerProfilePicture);
                } else {
                    ((ReceivedMessageHolder) holder).bind(message, null);
                }
        }
    }

    public boolean isLastInSequence(int position, Message currentMessage) {
        Message nextMessage = null;
        if (position < this.getItemCount() - 1) {
            nextMessage = this.getItem(position + 1);
        }
        if (nextMessage == null) {
            return true;
        } else if (!nextMessage.getUserId().equals(currentMessage.getUserId())) {
            return true;
        } else {
            return false;
        }
    }

    public Message getItem(int position) {
        return mMessageList.get(position);
    }
}

package com.bravetheworld.life.chat;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bravetheworld.R;
import com.bravetheworld.general.model.Message;

/**
 * Created by Konrad Sochacki on 13.12.2018.
 */
public class ReceivedMessageHolder extends RecyclerView.ViewHolder {
    TextView messageText;
    ImageView profileImage;

    ReceivedMessageHolder(View itemView) {
        super(itemView);
        messageText = itemView.findViewById(R.id.text_message_body);
        profileImage = itemView.findViewById(R.id.image_message_profile);
    }

    void bind(Message message, Bitmap photo) {
        messageText.setText(message.getText());
        if(photo != null) {
            profileImage.setImageBitmap(photo);
        }
    }
}
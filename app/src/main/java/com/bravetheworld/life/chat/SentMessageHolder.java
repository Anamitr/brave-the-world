package com.bravetheworld.life.chat;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bravetheworld.R;
import com.bravetheworld.general.model.Message;

/**
 * Created by Konrad Sochacki on 13.12.2018.
 */
public class SentMessageHolder extends RecyclerView.ViewHolder {
    TextView messageText;

    SentMessageHolder(View itemView) {
        super(itemView);
        messageText = itemView.findViewById(R.id.text_message_body);
    }

    void bind(Message message) {
        messageText.setText(message.getText());
    }
}
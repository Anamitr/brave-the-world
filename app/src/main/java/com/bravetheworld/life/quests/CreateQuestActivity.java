package com.bravetheworld.life.quests;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bravetheworld.R;
import com.bravetheworld.general.model.Quest;
import com.bravetheworld.general.model.User;
import com.bravetheworld.life.MatchesAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Map;
import java.util.TreeMap;

import static com.bravetheworld.life.quests.QuestActivity.MATCH_ID_KEY;


public class CreateQuestActivity extends AppCompatActivity {

    private String matchId;
    private User chattingPartner;

    private EditText titleEditText;
    private EditText descriptionEditText;
    private EditText pointsEditText;
    private Button createButton;

    DatabaseReference questsReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_quest);

        titleEditText = findViewById(R.id.title_edit_text);
        descriptionEditText = findViewById(R.id.description_edit_text);
        pointsEditText = findViewById(R.id.points_edit_text);
        createButton = findViewById(R.id.create_button);

        matchId = getIntent().getStringExtra(MATCH_ID_KEY);
        chattingPartner = (User) getIntent().getSerializableExtra(MatchesAdapter.CHATTING_PARTNER_KEY);

        questsReference = FirebaseDatabase.getInstance().getReference().child(getString(R.string.table_quests));

        setCreateButtonListener();

        insertExampleData();
    }

    private void setCreateButtonListener() {
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkIfAllFieldsNotEmpty();

                String questId = questsReference.child(matchId).push().getKey();
                String title = titleEditText.getText().toString();
                String description = descriptionEditText.getText().toString();
                Integer points = Integer.parseInt(pointsEditText.getText().toString());

                Quest newQuest = new Quest(questId, title, description, points);

                questsReference.child(matchId).child(questId).setValue(newQuest);

                finish();
            }
        });
    }

    private void checkIfAllFieldsNotEmpty() {
        Map<String, EditText> editTexts = new TreeMap<>();
        editTexts.put("title", titleEditText);
        editTexts.put("description", descriptionEditText);
        editTexts.put("points", pointsEditText);

        for (Map.Entry<String, EditText> entry : editTexts.entrySet()) {
            if (entry.getValue().getText().toString().isEmpty()) {
                Toast.makeText(getApplicationContext(), "You must set a " + entry.getKey() + " !", Toast.LENGTH_SHORT).show();
                return;
            }
        }
    }


    private void insertExampleData() {
        titleEditText.setText("Zadzwon do przyjaciela");
        descriptionEditText.setText("Zadzwon do przyjaciela i porozmawiaj przynajmniej 5 min");
        pointsEditText.setText("2");
    }
}
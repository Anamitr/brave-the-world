package com.bravetheworld.life.quests;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bravetheworld.R;
import com.bravetheworld.general.model.Quest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

/**
 * Created by Konrad Sochacki on 17.11.2018.
 */
public class QuestControlAdapter extends ArrayAdapter<Quest> {
    private static final String TAG = QuestControlAdapter.class.getSimpleName();

    DatabaseReference questsReference;
    String matchId;
    int resource;

    public QuestControlAdapter(@NonNull Context context, int resource, @NonNull List objects, String matchId) {
        super(context, resource, objects);
        this.matchId = matchId;
        this.resource = resource;
        questsReference = FirebaseDatabase.getInstance().getReference().child(getContext().getString(R.string.table_quests)).child(matchId);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) getContext()).getLayoutInflater().inflate(resource, parent, false);
        }

        TextView questInfoTextView = convertView.findViewById(R.id.quest_info_text_view);
        final Quest quest = getItem(position);
        questInfoTextView.setText(quest.getTitle() + " - " + quest.getPoints() + " pkt");

        ImageButton okButton = convertView.findViewById(R.id.ok_button);
        ImageButton noButton = convertView.findViewById(R.id.no_button);

        if(okButton != null && noButton != null) {
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    quest.setState(Quest.QuestStateEnum.COMPLETED);
                    questsReference.child(quest.getId()).setValue(quest);
                    QuestControlAdapter.this.remove(QuestControlAdapter.this.getItem(position));
                    QuestControlAdapter.this.notifyDataSetChanged();
                }
            });

            noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    quest.setState(Quest.QuestStateEnum.CANCELLED);
                    questsReference.child(quest.getId()).setValue(quest);
                    QuestControlAdapter.this.remove(QuestControlAdapter.this.getItem(position));
                    QuestControlAdapter.this.notifyDataSetChanged();
                }
            });
        }

        return convertView;
    }
}

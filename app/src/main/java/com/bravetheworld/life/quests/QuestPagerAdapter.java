package com.bravetheworld.life.quests;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.bravetheworld.general.BaseFragment;
import com.bravetheworld.general.model.RoleEnum;

/**
 * Created by Konrad Sochacki on 18.11.2018.
 */
public class QuestPagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = QuestPagerAdapter.class.getSimpleName();

    final int mNumOfTabs = 2;
    private String role;

    public QuestPagerAdapter(FragmentManager fm, String role) {
        super(fm);
        this.role = role;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                BaseFragment tab1 = null;
                if (role.equals(RoleEnum.JOYFUL_ONE.toString())) {
                    tab1 = new QuestsControlFragment();
                } else if (role.equals(RoleEnum.LONELY_ONE.toString())) {
                    tab1 = new QuestsDisplayFragment();
                } else {
                    Log.e(TAG, "Got wrong role = " + role);
                }
                return tab1;
            case 1:
                BaseFragment tab2 = new QuestArchivesFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

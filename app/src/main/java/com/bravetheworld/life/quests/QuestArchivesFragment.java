package com.bravetheworld.life.quests;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.bravetheworld.R;
import com.bravetheworld.general.BaseFragment;
import com.bravetheworld.general.model.Quest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;


public class QuestArchivesFragment extends BaseFragment {
    private static final String TAG = QuestArchivesFragment.class.getSimpleName();

    private QuestActivity questActivity;

    private ListView completedQuestsListView;
    private ListView cancelledQuestsListView;

    private String matchId;

    private DatabaseReference mQuestsReference;
    private ChildEventListener mArchivedQuestChildEventListener;

    ArchivedQuestAdapter mCompletedQuestAdapter;
    ArchivedQuestAdapter mCancelledQuestAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quest_archives, container, false);

        completedQuestsListView = view.findViewById(R.id.completed_quests_list_view);
        cancelledQuestsListView = view.findViewById(R.id.cancelled_quests_list_view);

        questActivity = (QuestActivity) getActivity();
        matchId = questActivity.matchId;


        List<Quest> completedQuestList = new ArrayList<>();
        List<Quest> cancellededQuestList = new ArrayList<>();
        mCompletedQuestAdapter = new ArchivedQuestAdapter(getContext(), R.layout.item_quest_archived, completedQuestList, matchId);
        mCancelledQuestAdapter = new ArchivedQuestAdapter(getContext(), R.layout.item_quest_archived, cancellededQuestList, matchId);
        completedQuestsListView.setAdapter(mCompletedQuestAdapter);
        cancelledQuestsListView.setAdapter(mCancelledQuestAdapter);

        mQuestsReference = FirebaseDatabase.getInstance().getReference().child(getString(R.string.table_quests)).child(matchId);

        loadQuestsFromFirebaseAndPutInQuestAdapters();

        questActivity.setListenerOnQuestItemClicked(completedQuestsListView);
        questActivity.setListenerOnQuestItemClicked(cancelledQuestsListView);

        return view;
    }

    private void loadQuestsFromFirebaseAndPutInQuestAdapters() {
        mArchivedQuestChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Quest quest = dataSnapshot.getValue(Quest.class);
                if (quest.getState().equals(Quest.QuestStateEnum.COMPLETED)) {
                    mCompletedQuestAdapter.add(quest);
                } else if(quest.getState().equals(Quest.QuestStateEnum.CANCELLED)) {
                    mCancelledQuestAdapter.add(quest);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Quest quest = dataSnapshot.getValue(Quest.class);
                if (quest.getState().equals(Quest.QuestStateEnum.COMPLETED)) {
                    mCompletedQuestAdapter.add(quest);
                } else if(quest.getState().equals(Quest.QuestStateEnum.CANCELLED)) {
                    mCancelledQuestAdapter.add(quest);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mQuestsReference.addChildEventListener(mArchivedQuestChildEventListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mQuestsReference.removeEventListener(mArchivedQuestChildEventListener);
    }

}
package com.bravetheworld.life.quests;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bravetheworld.R;
import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.model.Quest;
import com.bravetheworld.general.model.User;

import static com.bravetheworld.life.MatchesAdapter.CHATTING_PARTNER_KEY;

public class QuestActivity extends AppCompatActivity {
    private static final String TAG = QuestActivity.class.getSimpleName();

    public static final String MATCH_ID_KEY = "MATCH_ID_KEY";
    public static final String QUESTS_PARTNER_PROFILE_PICTURE_KEY = "QUESTS_PARTNER_PROFILE_PICTURE_KEY";

    protected String matchId;
    protected User chattingPartner;
    protected Bitmap chattingPartnerProfilePicture;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quest_control);

        matchId = getIntent().getStringExtra(MATCH_ID_KEY);
        chattingPartner = (User) getIntent().getSerializableExtra(CHATTING_PARTNER_KEY);
        chattingPartnerProfilePicture = getIntent().getParcelableExtra(QUESTS_PARTNER_PROFILE_PICTURE_KEY);

        initTabs();

        try {
            QuestPagerAdapter.class.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void initTabs() {
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Current"));
        tabLayout.addTab(tabLayout.newTab().setText("Archive"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = findViewById(R.id.pager);
        final QuestPagerAdapter adapter = new QuestPagerAdapter
                (getSupportFragmentManager(), ((ApplicationContext) getApplicationContext()).getAuthenticatedUser().getRole());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    protected void setListenerOnQuestItemClicked(ListView questListView) {
        questListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Quest quest = (Quest) adapterView.getItemAtPosition(position);
                displayQuestDialog(quest);
            }
        });
    }

    protected void displayQuestDialog(Quest quest) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(quest.getTitle())
                .setMessage(quest.getDescription() + " - " + quest.getPoints() + " pkt")
                .show();
    }
}

package com.bravetheworld.life.quests;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bravetheworld.R;
import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.model.Quest;
import com.bravetheworld.general.model.RoleEnum;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

/**
 * Created by Konrad Sochacki on 17.11.2018.
 */
public class ArchivedQuestAdapter extends ArrayAdapter<Quest> {
    private static final String TAG = ArchivedQuestAdapter.class.getSimpleName();

    DatabaseReference questsReference;
    String matchId;

    public ArchivedQuestAdapter(@NonNull Context context, int resource, @NonNull List objects, String matchId) {
        super(context, resource, objects);
        this.matchId = matchId;
        questsReference = FirebaseDatabase.getInstance().getReference().child(getContext().getString(R.string.table_quests)).child(matchId);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.item_quest_archived, parent, false);
        }

        TextView questInfoTextView = convertView.findViewById(R.id.quest_info_text_view);
        final Quest quest = getItem(position);
        questInfoTextView.setText(quest.getTitle() + " - " + quest.getPoints() + " pkt");

        ImageButton renewQuestButton = convertView.findViewById(R.id.renew_quest_button);

        if(((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser().getRole().equals(RoleEnum.JOYFUL_ONE.toString())) {
            renewQuestButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    quest.setState(Quest.QuestStateEnum.CREATED);
                    questsReference.child(quest.getId()).setValue(quest);
                    ArchivedQuestAdapter.this.remove(ArchivedQuestAdapter.this.getItem(position));
                    ArchivedQuestAdapter.this.notifyDataSetChanged();
                }
            });
        } else if (((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser().getRole().equals(RoleEnum.LONELY_ONE.toString())) {
            renewQuestButton.setVisibility(View.GONE);
        }

        return convertView;
    }
}
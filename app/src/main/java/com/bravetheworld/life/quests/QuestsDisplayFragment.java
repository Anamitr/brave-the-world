package com.bravetheworld.life.quests;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bravetheworld.R;
import com.bravetheworld.general.model.Quest;
import com.bravetheworld.general.model.QuestHeader;
import com.bravetheworld.general.model.User;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static com.bravetheworld.life.MatchesAdapter.CHATTING_PARTNER_KEY;
import static com.bravetheworld.life.quests.QuestActivity.MATCH_ID_KEY;

public class QuestsDisplayFragment extends QuestsControlFragment {
    private static final String TAG = QuestsDisplayFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.itemQuestLayout = R.layout.item_quest_display;
        View view = super.onCreateView(inflater, container, savedInstanceState);
        super.createQuestButton.setVisibility(View.GONE);
        return view;
    }

}
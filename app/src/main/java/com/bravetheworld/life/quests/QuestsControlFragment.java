package com.bravetheworld.life.quests;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bravetheworld.R;
import com.bravetheworld.general.BaseFragment;
import com.bravetheworld.general.model.Quest;
import com.bravetheworld.general.model.QuestHeader;
import com.bravetheworld.general.model.User;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static com.bravetheworld.life.MatchesAdapter.CHATTING_PARTNER_KEY;
import static com.bravetheworld.life.quests.QuestActivity.MATCH_ID_KEY;

public class QuestsControlFragment extends BaseFragment {
    private static final String TAG = QuestsControlFragment.class.getSimpleName();

    protected int itemQuestLayout = R.layout.item_quest_control;

    private QuestActivity questActivity;

    private TextView titleTextView, scoreTextView;
    private ImageView photoImageView;
    protected Button createQuestButton;
    private ListView questListView;

    private DatabaseReference mQuestsReference;
    private ChildEventListener mQuestChildEventListener;
    private DatabaseReference mQuestHeaderReference;
    private ValueEventListener questHeaderEventListener;

    private String matchId;
    private User chattingPartner;
    private Bitmap chattingPartnerProfilePicture;

    QuestControlAdapter mQuestControlAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quests_control, container, false);

        questActivity = (QuestActivity) getActivity();

        matchId = questActivity.matchId;
        chattingPartner = questActivity.chattingPartner;
        chattingPartnerProfilePicture = questActivity.chattingPartnerProfilePicture;

        titleTextView = view.findViewById(R.id.title_text_view);
        scoreTextView = view.findViewById(R.id.score_text_view);
        photoImageView = view.findViewById(R.id.photo_image_view);
        createQuestButton = view.findViewById(R.id.create_quest_button);
        questListView = view.findViewById(R.id.quests_list_view);

        photoImageView.setImageBitmap(chattingPartnerProfilePicture);

        createQuestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CreateQuestActivity.class);
                intent.putExtra(MATCH_ID_KEY, matchId);
                intent.putExtra(CHATTING_PARTNER_KEY, chattingPartner);
                startActivity(intent);
            }
        });

        List<Quest> questList = new ArrayList<>();
        mQuestControlAdapter = new QuestControlAdapter(getContext(), itemQuestLayout, questList, matchId);
        questListView.setAdapter(mQuestControlAdapter);

        mQuestsReference = FirebaseDatabase.getInstance().getReference().child(getString(R.string.table_quests)).child(matchId);
        mQuestHeaderReference = FirebaseDatabase.getInstance().getReference().child(getString(R.string.table_quest_headers)).child(matchId);

        loadQuestsFromFirebaseAndPutInQuestAdapter();
        setListenerForQuestHeader();

        questActivity.setListenerOnQuestItemClicked(questListView);

        return view;
    }

    private void loadQuestsFromFirebaseAndPutInQuestAdapter() {
        mQuestChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Quest quest = dataSnapshot.getValue(Quest.class);
                if (quest.getState().equals(Quest.QuestStateEnum.CREATED)) {
                    mQuestControlAdapter.add(quest);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Quest quest = dataSnapshot.getValue(Quest.class);
                if (quest.getState().equals(Quest.QuestStateEnum.CREATED)) {
                    mQuestControlAdapter.add(quest);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mQuestsReference.addChildEventListener(mQuestChildEventListener);
    }

    private void setListenerForQuestHeader() {
        questHeaderEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                QuestHeader questHeader = dataSnapshot.getValue(QuestHeader.class);
                if (questHeader == null) {
                    questHeader = createEmptyQuestHeader();
                }
                titleTextView.setText("Title:\t\t\t" + questHeader.getTitle());
                scoreTextView.setText("Score:\t" + questHeader.getPoints().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mQuestHeaderReference.addValueEventListener(questHeaderEventListener);

    }

    private QuestHeader createEmptyQuestHeader() {
        QuestHeader questHeader = new QuestHeader("Slave", 0);
        FirebaseDatabase.getInstance().getReference().child(getString(R.string.table_quest_headers)).child(matchId).setValue(questHeader);
        return questHeader;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mQuestsReference.removeEventListener(mQuestChildEventListener);
        mQuestHeaderReference.removeEventListener(questHeaderEventListener);
    }

}
package com.bravetheworld.life;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bravetheworld.R;
import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.Util;
import com.bravetheworld.general.model.Match;
import com.bravetheworld.general.model.RoleEnum;
import com.bravetheworld.general.model.User;
import com.bravetheworld.life.chat.ChatActivity;
import com.bravetheworld.life.quests.QuestActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import static com.bravetheworld.life.chat.ChatActivity.CHATTING_PARTNER_PROFILE_PICTURE_KEY;
import static com.bravetheworld.life.chat.ChatActivity.CHAT_ID_KEY;
import static com.bravetheworld.life.quests.QuestActivity.MATCH_ID_KEY;
import static com.bravetheworld.life.quests.QuestActivity.QUESTS_PARTNER_PROFILE_PICTURE_KEY;

/**
 * Created by Konrad Sochacki on 09.06.2018.
 */
public class MatchesAdapter extends ArrayAdapter<Match> {
    public static final String CHATTING_PARTNER_KEY = "CHATTING_PARTNER_KEY";
    private static final String TAG = MatchesAdapter.class.getSimpleName();

    private User mAuthenticatedUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mUsersDatabaseReference;

    User mChattingPartner = null;

    public MatchesAdapter(Context context, int list_item_user, List<Match> mMatchesList) {
        super(context, list_item_user, mMatchesList);
        mAuthenticatedUser = ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUsersDatabaseReference = mFirebaseDatabase.getReference().child(getContext().getString(R.string.table_users));
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.list_item_match, parent, false);
        }


        final ImageView profilePictureImageView = convertView.findViewById(R.id.photoImageView);
        final TextView userInfoTextView = convertView.findViewById(R.id.userInfoTextView);

        ImageButton chatButton = convertView.findViewById(R.id.chatButton);
        ImageButton questsButton = convertView.findViewById(R.id.questsButton);

        final Match match = getItem(position);
        if(match != null) {
            String oppositeUserId = null;
            mAuthenticatedUser = ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser();
            if (mAuthenticatedUser.role.equals(RoleEnum.JOYFUL_ONE.toString())) {
                oppositeUserId = match.lonelyOneId;
            } else if (mAuthenticatedUser.role.equals(RoleEnum.LONELY_ONE.toString())) {
                oppositeUserId = match.joyfulOneId;
            } else {
                Log.e(TAG, "Authenticated user role isn't lonely one nor joyful one!");
            }

            final User[] partner = {null};

            mUsersDatabaseReference.child(oppositeUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    mChattingPartner = user;
                    partner[0] = user;
                    userInfoTextView.setText(user.nick + ", " + user.age);
                    Util.loadProfilePicture(getContext(), profilePictureImageView, user);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            chatButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.v(TAG, "Open chat for match: " + match.id);
                    Intent intent = new Intent(getContext(), ChatActivity.class);
                    intent.putExtra(CHAT_ID_KEY, match.id);
                    intent.putExtra(CHATTING_PARTNER_KEY, partner[0]);
                    profilePictureImageView.buildDrawingCache();
                    intent.putExtra(CHATTING_PARTNER_PROFILE_PICTURE_KEY, profilePictureImageView.getDrawingCache());
                    getContext().startActivity(intent);
                }
            });
            questsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.v(TAG, "Open quests for match: " + match.id);
                    Intent intent = new Intent(getContext(), QuestActivity.class);
                    intent.putExtra(MATCH_ID_KEY, match.id);
                    intent.putExtra(CHATTING_PARTNER_KEY, partner[0]);
                    profilePictureImageView.buildDrawingCache();
                    intent.putExtra(QUESTS_PARTNER_PROFILE_PICTURE_KEY, profilePictureImageView.getDrawingCache());
                    getContext().startActivity(intent);
                }
            });
        }

        return convertView;
    }

}

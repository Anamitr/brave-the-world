package com.bravetheworld.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.bravetheworld.MainActivity;
import com.bravetheworld.R;
import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.model.Match;
import com.bravetheworld.general.model.Message;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Konrad Sochacki on 07.09.2018.
 */
public class NotificationsService extends Service {
    public static final String TAG = NotificationsService.class.getSimpleName();

    public static final String CHANNEL_ID = "brave_the_world_notifications_channel";

    private FirebaseAuth mFirebaseAuth;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mMatchesForUsersDatabaseReference;
    private DatabaseReference mMatchesDatabaseReference;
    private DatabaseReference mChatReference;

    private Map<String, Boolean> areMessagesLoaded;

    public NotificationsService(Context applicationContext) {
        super();
    }

    public NotificationsService() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.v(TAG, "Notifications service started");
//        createNotificationChannel();

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mMatchesForUsersDatabaseReference = mFirebaseDatabase.getReference().child(getApplicationContext().getString(R.string.table_matches_for_users));
        mMatchesDatabaseReference = mFirebaseDatabase.getReference().child(getApplicationContext().getString(R.string.table_matches));

        areMessagesLoaded = new HashMap<>();

//        setUpMatchesForUsersListener();

        return START_STICKY;
    }

    private void setUpListenerForMatch(final Match match) {
        mChatReference = FirebaseDatabase.getInstance().getReference().child(getString(R.string.table_chats)).child(match.id);

        areMessagesLoaded.put(match.id, false);

        mChatReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (areMessagesLoaded.get(match.id)) {
                    Message message = dataSnapshot.getValue(Message.class);
                    Log.v(TAG, "New message:" + message.getText());

                    if (!message.getUserId().equals(((ApplicationContext) getApplicationContext().getApplicationContext()).getAuthenticatedUser().nick))
                        displayNotification(message);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mChatReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.v(TAG, "Initial messages loaded");
                areMessagesLoaded.put(match.id, true);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setUpMatchesForUsersListener() {
        mMatchesForUsersDatabaseReference.child(mFirebaseAuth.getUid()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.v(TAG, "datasnapshot key: " + dataSnapshot.getKey());
                String matchForUser = dataSnapshot.getKey();
                mMatchesDatabaseReference.child(matchForUser).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Match match = dataSnapshot.getValue(Match.class);
                        setUpListenerForMatch(match);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void displayNotification(Message message) {
        Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
        mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, mainActivityIntent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(message.getUserId())
                .setContentText(message.getText())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());
        notificationManagerCompat.notify(1, mBuilder.build());
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.default_notification_channel);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "Notifications service onDestroy");
        Intent broadcastIntent = new Intent("RestartNotificationsService");
        sendBroadcast(broadcastIntent);
    }
}

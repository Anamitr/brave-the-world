package com.bravetheworld.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Konrad Sochacki on 07.09.2018.
 */
public class NotificationsRestarterBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = NotificationsRestarterBroadcastReceiver.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(TAG, "Restarting Notifications Service");
        context.startService(new Intent(context, NotificationsService.class));
    }
}

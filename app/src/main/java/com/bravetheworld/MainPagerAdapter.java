package com.bravetheworld;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.bravetheworld.general.BaseFragment;
import com.bravetheworld.life.LifeFragment;
import com.bravetheworld.settings.SettingsFragment;
import com.bravetheworld.world.WorldFragment;

/**
 * Created by Konrad Sochacki on 15.01.2018.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {
    int mNumOfTabs;

    public MainPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                BaseFragment tab1 = new WorldFragment();
                return tab1;
            case 1:
                BaseFragment tab2 = new LifeFragment();
                return tab2;
            case 2:
                SettingsFragment tab3 = new SettingsFragment();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
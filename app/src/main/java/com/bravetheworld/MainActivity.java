package com.bravetheworld;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.Toast;

import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.BaseActivity;
import com.bravetheworld.general.model.User;
import com.bravetheworld.start.SignInActivity;
import com.bravetheworld.world.WorldListViewsController;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.greenrobot.eventbus.EventBus;

public class MainActivity extends BaseActivity implements FirebaseAuth.AuthStateListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    public static final int RC_SIGN_IN = 1;

    public String deviceToken;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private TabLayout.OnTabSelectedListener mTabLayoutListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        initTabs();
        FirebaseAuth.getInstance().addAuthStateListener(this);
    }

    private void initTabs() {
        mTabLayout = generateTabLayout();
        mViewPager = generateViewPager();
        mTabLayoutListener = getNewTabLayoutListener(mViewPager);
        mTabLayout.addOnTabSelectedListener(getNewTabLayoutListener(mViewPager));
    }

    private TabLayout generateTabLayout() {
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("World"));
        tabLayout.addTab(tabLayout.newTab().setText("Life"));
        tabLayout.addTab(tabLayout.newTab().setText("Settings"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        return tabLayout;
    }

    private ViewPager generateViewPager() {
        final ViewPager viewPager = findViewById(R.id.pager);
        final MainPagerAdapter adapter = new MainPagerAdapter
                (getSupportFragmentManager(), mTabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        viewPager.setOffscreenPageLimit(adapter.getCount());
        return viewPager;
    }

    private TabLayout.OnTabSelectedListener getNewTabLayoutListener(final ViewPager viewPager) {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        };
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser != null) {
            // user is signed in
            FirebaseDatabase.getInstance().getReference().child(getString(R.string.table_users))
                    .child(firebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User authenticatedUser = dataSnapshot.getValue(User.class);
                    initUserDataAfterSignIn(authenticatedUser);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } else {
            // user is signed out

            WorldListViewsController.reset();
            startActivity(new Intent(MainActivity.this, SignInActivity.class));
            finish();
        }
    }

    private void initUserDataAfterSignIn(User authenticatedUser) {
        ((ApplicationContext) getApplicationContext()).setAuthenticatedUser(authenticatedUser);
        ((ApplicationContext) getApplicationContext()).loadAuthenticatedUserProfilePicture();
        sendDeviceTokenToServer();
        EventBus.getDefault().postSticky(new AuthenticatedUserDataSetInApplicationContextEvent());

        Toast.makeText(MainActivity.this, "You are now signed in. Welcome to Brave The World!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        FirebaseAuth.getInstance().removeAuthStateListener(this);
        mTabLayout.removeOnTabSelectedListener(mTabLayoutListener);
        super.onDestroy();
    }

    private void sendDeviceTokenToServer() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        String token = task.getResult().getToken();
                        Log.d(TAG, "Sending instance token = " + token);
                        deviceToken = token;
                        User authenticatedUser = ((ApplicationContext) getApplicationContext()).getAuthenticatedUser();
                        if (authenticatedUser != null) {
                            authenticatedUser.setDeviceToken(deviceToken);
                            FirebaseDatabase.getInstance().getReference().child(getString(R.string.table_users)).child(authenticatedUser.id).child("deviceToken").setValue(token);
                        } else {
                            Log.e(TAG, "Authenticated user == null");
                        }

                    }
                });
    }

    public class AuthenticatedUserDataSetInApplicationContextEvent {
    }
}

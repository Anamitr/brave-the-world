package com.bravetheworld.settings;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bravetheworld.MainActivity;
import com.bravetheworld.R;
import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.BaseFragment;
import com.bravetheworld.general.Util;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.FileNotFoundException;
import java.io.InputStream;

import static android.app.Activity.RESULT_OK;

public class SettingsFragment extends BaseFragment {

    private static final String TAG = SettingsFragment.class.getSimpleName();
    private static int RESULT_LOAD_IMG = 1;

    Button buttonSignOut;
    Button buttonUploadProfilePicture;
    TextView loggedInUsernameTextView;
    ImageView imageViewProfilePicture;
    //TODO: Bug - progress bar frozen when SettingsFragment clicked right after app start
    private ProgressBar mProgressBar;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseStorage mFirebaseStorage;
    private StorageReference profilePictureReference;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseStorage = FirebaseStorage.getInstance();
        profilePictureReference = mFirebaseStorage.getReference().child("profile/" + mFirebaseAuth.getUid() + ".jpg");

        buttonSignOut = view.findViewById(R.id.button_sign_out);
        buttonUploadProfilePicture = view.findViewById(R.id.button_upload_profile_picture);
        loggedInUsernameTextView = view.findViewById(R.id.logged_in_username_text_view);
        imageViewProfilePicture = view.findViewById(R.id.image_view_profile_picture);
        mProgressBar = view.findViewById(R.id.progressBar);

        mProgressBar.setVisibility(ProgressBar.VISIBLE);


        if(isAuthenticatedUserInContext()) {
            loggedInUsernameTextView.setText(((ApplicationContext) getActivity().getApplicationContext()).getAuthenticatedUser().nick);
        }

        buttonSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFirebaseAuth.signOut();
            }
        });

        buttonUploadProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
            }
        });

        if (isProfilePictureLoaded()) {
            imageViewProfilePicture.setImageBitmap(((getApplicationContext()).getProfilePicture()));
            mProgressBar.setVisibility(View.INVISIBLE);
        }

        return view;
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                final Bitmap imageBitmap = BitmapFactory.decodeStream(imageStream);
                imageViewProfilePicture.setImageBitmap(imageBitmap);
                ((ApplicationContext) getActivity().getApplicationContext()).setProfilePicture(imageBitmap);

                final byte[] imageData = Util.compressBitmap(imageBitmap);

                final UploadTask uploadTask = profilePictureReference.putBytes(imageData);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        if (isAdded()) {
                            Toast.makeText(getActivity(), "Upload task failed! " + exception.toString(), Toast.LENGTH_SHORT).show();
                            uploadTask.toString();
                            ((ApplicationContext) getActivity().getApplicationContext())
                                    .setProfilePicture(BitmapFactory.decodeByteArray(imageData, 0, imageData.length));
                        }
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        if (isAdded()) {
                            Toast.makeText(getActivity(), "Profile picture updated!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(getActivity(), "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }

    @Subscribe(sticky = true)
    public void onProfilePictureDownloaded(ApplicationContext.ProfilePictureDownloadFinishEvent profilePictureDownloadFinishEvent) {
        Log.v(TAG, "Profile picture has been downlaoded");
        imageViewProfilePicture.setImageBitmap(((ApplicationContext) getActivity().getApplicationContext()).getProfilePicture());
        mProgressBar.setVisibility(View.INVISIBLE);
        EventBus.getDefault().removeStickyEvent(profilePictureDownloadFinishEvent);
    }

    @Subscribe(sticky = true)
    public void onAuthenticatedUserDataSetInApplicationContextEvent(MainActivity.AuthenticatedUserDataSetInApplicationContextEvent authenticatedUserDataSetInApplicationContextEvent) {
        Log.v(TAG, "AuthenticatedUserDataSetInApplicationContextEvent");
        loggedInUsernameTextView.setText(((ApplicationContext) getActivity().getApplicationContext()).getAuthenticatedUser().nick);
        EventBus.getDefault().removeStickyEvent(authenticatedUserDataSetInApplicationContextEvent);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

}
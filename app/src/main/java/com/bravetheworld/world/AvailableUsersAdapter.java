package com.bravetheworld.world;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bravetheworld.R;
import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.Util;
import com.bravetheworld.general.model.Request;
import com.bravetheworld.general.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

import static android.content.ContentValues.TAG;

public class AvailableUsersAdapter extends ArrayAdapter<User> {

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mRequestsSentDatabaseReference;
    private DatabaseReference mRequestsReceivedDatabaseReference;
    private FirebaseAuth mFirebaseAuth;
    private User mAuthenticatedUser;
    private WorldListViewsController mWorldAdapterController;

    public AvailableUsersAdapter(Context context, int resource, List<User> objects, WorldListViewsController worldAdapterController) {
        super(context, resource, objects);
        mAuthenticatedUser = ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser();
        mWorldAdapterController = worldAdapterController;

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mRequestsSentDatabaseReference = mFirebaseDatabase.getReference().child(getContext().getString(R.string.table_requests_sent));
        mRequestsReceivedDatabaseReference = mFirebaseDatabase.getReference().child(getContext().getString(R.string.table_requests_received));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_user, parent, false);
        }

        ImageView profilePictureImageView = convertView.findViewById(R.id.photoImageView);
        TextView userInfoTextView = convertView.findViewById(R.id.user_info_text_view);
        ImageButton okButton = convertView.findViewById(R.id.ok_button);
        ImageButton noButton = convertView.findViewById(R.id.no_button);

        final User user = getItem(position);

        Util.loadProfilePicture(getContext(), profilePictureImageView, user);
        userInfoTextView.setText(user.nick + ", " + user.age);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String authenticatedUserId = mFirebaseAuth.getCurrentUser().getUid();
                mRequestsSentDatabaseReference.child(authenticatedUserId).child(user.id).setValue(new Request(true,
                        ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser().id, user.id));
                mRequestsSentDatabaseReference.child(user.id).child(authenticatedUserId).setValue(new Request(true,
                        ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser().id, user.id));
                mRequestsReceivedDatabaseReference.child(user.id).child(authenticatedUserId).setValue(new Request(true,
                        ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser().id, user.id));

                Log.v(TAG, "Send request from " + ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser().nick + " to " + user.nick);

                mWorldAdapterController.reloadAvailableUsers();
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String authenticatedUserId = mFirebaseAuth.getCurrentUser().getUid();
                mRequestsSentDatabaseReference.child(authenticatedUserId).child(user.id).setValue(new Request(false,
                        ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser().id, user.id));

                Log.v(TAG, "Send no request from " + ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser().nick + " to " + user.nick);

                mWorldAdapterController.reloadAvailableUsers();
            }
        });

        return convertView;
    }


}

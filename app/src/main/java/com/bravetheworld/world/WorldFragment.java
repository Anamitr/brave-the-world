package com.bravetheworld.world;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.bravetheworld.R;
import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.BaseFragment;
import com.bravetheworld.services.NotificationsService;

/**
 * Created by Konrad Sochacki on 27.01.2018.
 */

public class WorldFragment extends BaseFragment {
    private static final String TAG = WorldFragment.class.getSimpleName();

    private ListView availableUsersListView;
    private ListView requestsReceivedListView;
    private ProgressBar mProgressBar;

    // for developer purposes
    Button testButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_world, container, false);
        availableUsersListView = view.findViewById(R.id.availableUsersListView);
        requestsReceivedListView = view.findViewById(R.id.requestsReceivedListView);
        mProgressBar = view.findViewById(R.id.progressBar);
//        testButton = view.findViewById(R.id.testButton);

        WorldListViewsController.getInstance(((ApplicationContext) getActivity().getApplicationContext()), mProgressBar).setupWithWorldFragment(this);

//        setTestButtonListener();

        return view;
    }

    private void setTestButtonListener() {
        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                reloadAvailableUsers();
//                reloadInvites();
                Log.v(TAG, "is Notifications Service running = " + isNotificationsServiceRunning(NotificationsService.class));
//                mRequestsReceivedAdapter.clear();
//                mAvailableUsersAdapter.clear();
            }
        });
    }
    private boolean isNotificationsServiceRunning(Class<? extends NotificationsService> serviceClass) {
        ActivityManager activityManager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for(ActivityManager.RunningServiceInfo serviceInfo : activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if(serviceClass.getName().equals(serviceInfo.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public ListView getAvailableUsersListView() {
        return availableUsersListView;
    }

    public ListView getRequestsReceivedListView() {
        return requestsReceivedListView;
    }

    public ProgressBar getProgressBar() { return mProgressBar; }
}

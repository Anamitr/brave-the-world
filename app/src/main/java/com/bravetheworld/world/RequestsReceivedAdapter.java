package com.bravetheworld.world;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bravetheworld.R;
import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.Util;
import com.bravetheworld.general.model.ChatHeader;
import com.bravetheworld.general.model.Match;
import com.bravetheworld.general.model.QuestHeader;
import com.bravetheworld.general.model.Request;
import com.bravetheworld.general.model.RoleEnum;
import com.bravetheworld.general.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import static android.content.ContentValues.TAG;

public class RequestsReceivedAdapter extends ArrayAdapter<User> {

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mRequestsSentDatabaseReference;
    private DatabaseReference mRequestsReceivedDatabaseReference;
    private DatabaseReference mMatchesForUsersDatabaseReference;
    private DatabaseReference mMatchesDatabaseReference;
    private DatabaseReference mChatsDatabaseReference;
    private FirebaseAuth mFirebaseAuth;
    private User mAuthenticatedUser;
    private WorldListViewsController mWorldAdapterController;

    public RequestsReceivedAdapter(Context context, int resource, List<User> objects, WorldListViewsController worldAdapterController) {
        super(context, resource, objects);
        mAuthenticatedUser = ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser();
        mWorldAdapterController = worldAdapterController;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_user, parent, false);
        }

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mRequestsSentDatabaseReference = mFirebaseDatabase.getReference().child(getContext().getString(R.string.table_requests_sent));
        mRequestsReceivedDatabaseReference = mFirebaseDatabase.getReference().child(getContext().getString(R.string.table_requests_received));
        mMatchesForUsersDatabaseReference = mFirebaseDatabase.getReference().child(getContext().getString(R.string.table_matches_for_users));
        mMatchesDatabaseReference = mFirebaseDatabase.getReference().child(getContext().getString(R.string.table_matches));
        mChatsDatabaseReference = mFirebaseDatabase.getReference().child(getContext().getString(R.string.table_chats));

        ImageView profilePictureImageView = convertView.findViewById(R.id.photoImageView);
        TextView userInfoTextView = convertView.findViewById(R.id.user_info_text_view);
        ImageButton okButton = convertView.findViewById(R.id.ok_button);
        ImageButton noButton = convertView.findViewById(R.id.no_button);

        final User user = getItem(position);

        Util.loadProfilePicture(getContext(), profilePictureImageView, user);
        userInfoTextView.setText(user.nick + ", " + user.age);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String authenticatedUserId = mFirebaseAuth.getCurrentUser().getUid();
                mRequestsSentDatabaseReference.child(authenticatedUserId).child(user.id).setValue(new Request(true,
                        ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser().id, user.id));
                mRequestsReceivedDatabaseReference.child(authenticatedUserId).child(user.id).removeValue();
                mMatchesForUsersDatabaseReference.child(user.id).push().setValue(user.id);

                mMatchesForUsersDatabaseReference.child(user.id).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        String matchKey = dataSnapshot.getKey();
                        mMatchesForUsersDatabaseReference.child(authenticatedUserId).child(matchKey).setValue(authenticatedUserId);
                        mChatsDatabaseReference.child(matchKey).setValue(matchKey);
                        String lonelyOneId = null, joyfulOneId = null;
                        mAuthenticatedUser = ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser();
                        if (mAuthenticatedUser.role.equals(RoleEnum.LONELY_ONE.toString())) {
                            lonelyOneId = authenticatedUserId;
                            joyfulOneId = user.id;
                        } else if (mAuthenticatedUser.role.equals(RoleEnum.JOYFUL_ONE.toString())) {
                            joyfulOneId = authenticatedUserId;
                            lonelyOneId = user.id;
                        } else {
                            Log.e(TAG, "Authenticated user role isn't lonely one nor joyful one!");
                        }
                        if (lonelyOneId != null && joyfulOneId != null) {
                            Match match = new Match(matchKey, lonelyOneId, joyfulOneId, 0);
                            mMatchesDatabaseReference.child(matchKey).setValue(match);

                            QuestHeader questHeader = new QuestHeader("Slave", 0);
                            FirebaseDatabase.getInstance().getReference().child(getContext().getString(R.string.table_quest_headers)).child(matchKey).setValue(questHeader);

                            ChatHeader chatHeader = new ChatHeader(matchKey, lonelyOneId, joyfulOneId);
                            FirebaseDatabase.getInstance().getReference().child(getContext().getString(R.string.table_chat_headers)).child(matchKey).setValue(chatHeader);
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                Log.v(TAG, "Set match: " + authenticatedUserId + " - " + user.id);

                mWorldAdapterController.reloadInvites();
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String authenticatedUserId = mFirebaseAuth.getCurrentUser().getUid();
                mRequestsSentDatabaseReference.child(user.id).child(authenticatedUserId).setValue(new Request(false,
                        ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser().id, user.id));

                Log.v(TAG, "Set no request from " + user.nick + " to " + ((ApplicationContext) getContext().getApplicationContext()).getAuthenticatedUser().nick);

                mWorldAdapterController.reloadInvites();
            }
        });

        return convertView;
    }
}

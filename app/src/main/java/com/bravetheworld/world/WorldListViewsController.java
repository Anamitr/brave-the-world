package com.bravetheworld.world;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.bravetheworld.R;
import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.model.Request;
import com.bravetheworld.general.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class WorldListViewsController {
    private static final String TAG = WorldListViewsController.class.getSimpleName();

    private static WorldListViewsController instance = null;

    private ApplicationContext mApplicationContext;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseDatabase mFirebaseDatabase;

    private AvailableUsersAdapter mAvailableUsersAdapter;
    private RequestsReceivedAdapter mRequestsReceivedAdapter;

    private List<String> mAvailableUsersIds;
    private List<User> mAvailableUsersList;
    private List<User> mUserInvitesList;
    private List<Request> mRequestsSentList;
    private List<Request> mRequestsReceivedList;

    private DatabaseReference mUsersDatabaseReference;
    private DatabaseReference mRequestsSentDatabaseReference;
    private DatabaseReference mRequestsReceivedDatabaseReference;

    private WorldFragment mWorldFragment;
    private ProgressBar worldFragmentProgressBar;

    private WorldListViewsController(@NonNull ApplicationContext applicationContext, ProgressBar worldFragmentProgressBar) {
        this.mApplicationContext = applicationContext;
        this.worldFragmentProgressBar = worldFragmentProgressBar;

        mFirebaseAuth = FirebaseAuth.getInstance();

        mAvailableUsersIds = new ArrayList<>();
        mAvailableUsersList = new ArrayList<>();
        mUserInvitesList = new ArrayList<>();
        mRequestsSentList = new ArrayList<>();
        mRequestsReceivedList = new ArrayList<>();

        mWorldFragment = null;

        if (mFirebaseAuth.getUid() != null) {
            initDatabaseReferences();
            initAdapters();
            loadData();
        }
    }

    private void loadData() {
        loadSentRequests();
        loadRequestsReceived();
    }

    private void initAdapters() {
        initAvailableUsersAdapter();
        initRequestsReceivedAdapter();
    }

    private void initDatabaseReferences() {
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUsersDatabaseReference = mFirebaseDatabase.getReference().child(mApplicationContext.getString(R.string.table_users));
        mRequestsSentDatabaseReference = mFirebaseDatabase.getReference().child(mApplicationContext.getString(R.string.table_requests_sent))
                .child(mFirebaseAuth.getUid());
        //.child("jlR9RYKqtXNjDnwJh8FOWw04BXk1");
        mRequestsReceivedDatabaseReference = mFirebaseDatabase.getReference().child(mApplicationContext.getString(R.string.table_requests_received))
                .child(mFirebaseAuth.getUid());
    }

    private void initAvailableUsersAdapter() {
        mAvailableUsersAdapter = new AvailableUsersAdapter(mApplicationContext, R.layout.list_item_user, mAvailableUsersList, this);
    }

    private void initRequestsReceivedAdapter() {
        mRequestsReceivedAdapter = new RequestsReceivedAdapter(mApplicationContext, R.layout.list_item_user, mUserInvitesList, this);
    }

    protected void reloadAvailableUsers() {
        loadSentRequests();
    }

    private void loadSentRequests() {
        worldFragmentProgressBar.setVisibility(View.VISIBLE);
        mRequestsSentDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot requestSnapshot : dataSnapshot.getChildren()) {
                    Request request = requestSnapshot.getValue(Request.class);
                    mRequestsSentList.add(request);
                }
                Log.v(TAG, "requests");
                for (Request request : mRequestsSentList) {
                    Log.v(TAG, request.toString());
                }
                // We need to have requestSent list loaded first
                // Not very elegant
                setUpAvailableUsersListener();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setUpAvailableUsersListener() {
        mAvailableUsersAdapter.clear();
//        worldFragmentProgressBar.setVisibility(View.VISIBLE);
        ChildEventListener availableUsersListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                User user = dataSnapshot.getValue(User.class);
                Log.v(TAG, dataSnapshot.getKey() + ": " + user.nick);

                mAvailableUsersIds.add(dataSnapshot.getKey());
//                mAvailableUsersList.add(user);

                if (!isCurrentlyAuthenticatedUser(user) && isOppositeRole(user) && !isRequestAlreadySent(user) && !haveReceivedRequestFromUser(user)) {
                    mAvailableUsersAdapter.add(user);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };

        mUsersDatabaseReference.addChildEventListener(availableUsersListener);

        mUsersDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //TODO: should check whether requests loaded as well, but no difference as long as num of requests lower than num of available users
                Log.v(TAG, "Available users loaded");
                worldFragmentProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private boolean isRequestAlreadySent(User user) {
        if (mRequestsSentList != null) {
            for (Request request : mRequestsSentList) {
                if (request.toUserId.equals(user.id)) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    private boolean isOppositeRole(User user) {
        return !mApplicationContext.getAuthenticatedUser().role.equals(user.role);
    }

    private boolean isCurrentlyAuthenticatedUser(User user) {
        return user.id.equals(mApplicationContext.getAuthenticatedUser().id);
    }

    // Received invites

    protected void reloadInvites() {
        loadRequestsReceived();
    }

    private void loadRequestsReceived() {
        mRequestsReceivedList = new ArrayList<>();
        mRequestsReceivedDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot requestSnapshot : dataSnapshot.getChildren()) {
                    Request request = requestSnapshot.getValue(Request.class);
                    mRequestsReceivedList.add(request);
                }
                Log.v(TAG, "Requests received:");
                for (Request request : mRequestsReceivedList) {
                    Log.v(TAG, request.toString());
                }
                // We need to have requestReceived list loaded first
                // Not very elegant
                setUpRequestsReceivedListener();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setUpRequestsReceivedListener() {
        mRequestsReceivedAdapter.clear();
        mUsersDatabaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                User user = dataSnapshot.getValue(User.class);
                Log.v(TAG, dataSnapshot.getKey() + ": " + user.nick);

//                mAvailableUsersIds.add(dataSnapshot.getKey());
//                mAvailableUsersList.add(user);

                if (haveReceivedRequestFromUser(user)) {
                    mRequestsReceivedAdapter.add(user);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private boolean haveReceivedRequestFromUser(User user) {
        if (mRequestsReceivedList != null) {
            for (Request request : mRequestsReceivedList) {
                if (request.fromUserId.equals(user.id)) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    protected void setupWithWorldFragment(WorldFragment worldFragment) {
        mWorldFragment = worldFragment;
        mWorldFragment.getAvailableUsersListView().setAdapter(mAvailableUsersAdapter);
        mWorldFragment.getRequestsReceivedListView().setAdapter(mRequestsReceivedAdapter);
        worldFragmentProgressBar = mWorldFragment.getProgressBar();
    }

    public static WorldListViewsController getInstance(ApplicationContext applicationContext, ProgressBar worldFragmentProgressBar) {
        if (instance == null) {
            instance = new WorldListViewsController(applicationContext, worldFragmentProgressBar);
        }
        return instance;
    }

    public static void reset() {
        instance = null;
    }

}

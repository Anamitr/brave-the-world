package com.bravetheworld.start;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import com.bravetheworld.R;
import com.bravetheworld.general.ApplicationContext;
import com.bravetheworld.general.BaseActivity;
import com.bravetheworld.general.model.RoleEnum;
import com.bravetheworld.general.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

public class SignUpActivity extends BaseActivity {
    private static final String TAG = SignUpActivity.class.getSimpleName();

    private EditText editTextEmail, editTextPassword, editTextNick, editTextCity, editTextAge;
    private ProgressBar progressBar;
    private Switch switchRole;
    private Button buttonSignUp;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mUsersDatabaseReference;
    private FirebaseStorage mFirebaseStorage;
    private StorageReference mProfilePicturesReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUsersDatabaseReference = mFirebaseDatabase.getReference().child(getString(R.string.table_users));
        mFirebaseStorage = FirebaseStorage.getInstance();


        editTextEmail = findViewById(R.id.edit_email);
        editTextPassword = findViewById(R.id.edit_password);
        editTextNick = findViewById(R.id.edit_nick);
        editTextCity = findViewById(R.id.edit_city);
        editTextAge = findViewById(R.id.edit_age);
        progressBar = findViewById(R.id.progress_bar);
        switchRole = findViewById(R.id.switch_role);
        buttonSignUp = findViewById(R.id.button_sign_up);

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String email = editTextEmail.getText().toString().trim();
                String password = editTextPassword.getText().toString().trim();
                final String nick = editTextNick.getText().toString().trim();
                final String city = editTextCity.getText().toString().trim();
                final String age = editTextAge.getText().toString().trim();
                final String role = getRole();
//                Log.v(TAG, "role = " + role);

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(nick)) {
                    Toast.makeText(getApplicationContext(), "Enter nick!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(city)) {
                    Toast.makeText(getApplicationContext(), "Enter city!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(age)) {
                    Toast.makeText(getApplicationContext(), "Enter age!", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);


                mFirebaseAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                progressBar.setVisibility(View.GONE);
                                if (!task.isSuccessful()) {
                                    toastTaskException(task);
                                } else {
                                    String userId = mFirebaseAuth.getCurrentUser().getUid();
                                    final User newUser = new User(userId, email, nick, age, city, role);
                                    mUsersDatabaseReference.child(userId).setValue(newUser);

                                    finishWithResultOk();
                                }
                            }
                        });

            }
        });

        //insertMagdaCredentials();
//        insertExampleData();
    }

    @NonNull
    private String getRole() {
        String role;
        if (switchRole.isChecked()) {
            role = switchRole.getTextOn().toString();
        } else {
            role = switchRole.getTextOff().toString();
        }
        return role;
    }

    // For debug only
    private void insertExampleData() {
        editTextEmail.setText("sochacki.konrad@gmail.com");
        editTextPassword.setText("qwerty");
        editTextNick.setText("Anamitr");
        editTextCity.setText("Pabianice");
        editTextAge.setText("22");
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }

    private void insertMagdaCredentials() {
        editTextEmail.setText("magdalena.sochacka4@gmail.com");
        editTextNick.setText("Magda");
        editTextPassword.setText("qwerty");
        editTextCity.setText("Pabianice");
        editTextAge.setText("48");
        switchRole.setChecked(true);
    }
}

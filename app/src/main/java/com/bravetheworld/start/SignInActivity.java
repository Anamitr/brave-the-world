package com.bravetheworld.start;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bravetheworld.MainActivity;
import com.bravetheworld.R;
import com.bravetheworld.general.BaseActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignInActivity extends BaseActivity {
    private static final String TAG = SignInActivity.class.getSimpleName();

    private EditText editTextEmail, editTextPassword;
    private ProgressBar progressBar;
    private Button buttonLogin, buttonSignUp;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() != null) {
            finishWithResultOk();
        }

        editTextEmail = (EditText) findViewById(R.id.edit_email);
        editTextPassword = (EditText) findViewById(R.id.edit_password);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        buttonLogin = (Button) findViewById(R.id.button_login);
        buttonSignUp = (Button) findViewById(R.id.button_sign_up);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = editTextEmail.getText().toString();
                final String password = editTextPassword.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                firebaseAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(SignInActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                progressBar.setVisibility(View.GONE);
                                if (!task.isSuccessful()) {
                                    Toast.makeText(SignInActivity.this, getString(R.string.authentication_failed), Toast.LENGTH_SHORT).show();
                                } else {
                                    startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                    finish();
                                }
                            }
                        });
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
            }
        });

//        insertAsiaCredentials();
//        insertKonradCredentials();
//        insertBenekCredentials();
//        insertMarysiaCredentials();
    }

    // For debug only
    private void insertKonradCredentials() {
        editTextEmail.setText("sochacki.konrad@gmail.com");
        editTextPassword.setText("qwerty");
    }
    // For debug only
    private void insertAsiaCredentials() {
        editTextEmail.setText("asia.wendler@gmail.com");
        editTextPassword.setText("qwerty");
    }
    private void insertBenekCredentials() {
        editTextEmail.setText("b@g.pl");
        editTextPassword.setText("qwerty");
    }
    private void insertMarysiaCredentials() {
        editTextEmail.setText("maria.sobieska@gmail.com");
        editTextPassword.setText("qwerty");
    }



}

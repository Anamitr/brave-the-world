package com.bravetheworld.general;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import com.bravetheworld.R;
import com.bravetheworld.general.model.RoleEnum;
import com.bravetheworld.general.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static android.content.ContentValues.TAG;

/**
 * Created by Konrad Sochacki on 03.10.2018.
 */
public class Util {
    public static final boolean SHOULD_DOWNLOAD_PICTURES = true;
    public static final int MAX_PHOTO_SIZE = 1024 * 1024;

    public static Bitmap getEmptyProfilePicture(Context context, User user) {
        Bitmap result = null;
        if (user.role.equals(RoleEnum.JOYFUL_ONE.toString())) {
            result = BitmapFactory.decodeResource(context.getResources(), R.mipmap.empty_profile_female_2);
        } else if (user.role.equals(RoleEnum.LONELY_ONE.toString())) {
            result = BitmapFactory.decodeResource(context.getResources(), R.mipmap.empty_profile_male_2);
        }
        return result;
    }

    public static byte[] compressBitmap(Bitmap bitmap) {
        int streamLength = MAX_PHOTO_SIZE;
        int compressQuality = 105;
        ByteArrayOutputStream bmpStream = new ByteArrayOutputStream();
        byte[] bmpPicByteArray = null;
        while (streamLength >= MAX_PHOTO_SIZE && compressQuality > 5) {
            try {
                bmpStream.flush();//to avoid out of memory error
                bmpStream.reset();
            } catch (IOException e) {
                e.printStackTrace();
            }
            compressQuality -= 5;
            bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream);
            bmpPicByteArray = bmpStream.toByteArray();
            streamLength = bmpPicByteArray.length;
            //if (BuildConfig.DEBUG) {
            Log.d("test upload", "Quality: " + compressQuality);
            Log.d("test upload", "Size: " + streamLength);
            //}
        }
        return bmpPicByteArray;
    }

    public static void loadProfilePicture(final Context context, final ImageView profilePictureImageView, final User user) {
        profilePictureImageView.setImageBitmap(Util.getEmptyProfilePicture(context, user));
        if (Util.SHOULD_DOWNLOAD_PICTURES) {
            StorageReference chattingPartnerProfilePictureReference = FirebaseStorage.getInstance().getReference().child("profile/" + user.id + ".jpg");
            chattingPartnerProfilePictureReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.get().load(uri).into(profilePictureImageView);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "Available user profile picture not found");
                }
            });
        }
    }
}

package com.bravetheworld.general;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;


/**
 * Created by Konrad Sochacki on 02.01.2018.
 */

public class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getSimpleName();

    protected void finishWithResultOk() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    protected void toastTaskException(Task task) {
        Toast.makeText(this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
    }
}
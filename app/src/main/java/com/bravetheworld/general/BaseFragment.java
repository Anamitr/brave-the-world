package com.bravetheworld.general;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bravetheworld.R;
import com.bravetheworld.start.SignInActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import static android.content.ContentValues.TAG;
import static com.bravetheworld.MainActivity.RC_SIGN_IN;

public class BaseFragment extends Fragment {


    protected FirebaseDatabase mFirebaseDatabase;

    public void refresh() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }

    public boolean isAuthenticatedUserInContext() {
        return ((ApplicationContext) getActivity().getApplicationContext()).getAuthenticatedUser() != null;
    }

    public boolean isProfilePictureLoaded() {
        return ((ApplicationContext) getActivity().getApplicationContext()).getProfilePicture() != null;
    }

    public ApplicationContext getApplicationContext() {
        return (ApplicationContext) getActivity().getApplicationContext();
    }

    public boolean isUserSignedIn() {
        return FirebaseAuth.getInstance().getUid() != null;
    }
}

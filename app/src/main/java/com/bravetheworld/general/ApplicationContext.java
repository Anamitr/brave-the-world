package com.bravetheworld.general;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import com.bravetheworld.general.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Konrad Sochacki on 28.02.2018.
 */

public class ApplicationContext extends Application {
    private static final String TAG = ApplicationContext.class.getSimpleName();

    private FirebaseAuth mFirebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private User authenticatedUser;

    private FirebaseStorage mFirebaseStorage;
    private StorageReference profilePictureReference;
    private Bitmap profilePicture;

    // Need to keep strong reference for this, because otherwise it might be garbage collected
    // before Picasso downloads picture
    Target targetForProfilePictureDownload;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "Initializing custom ApplicationContext");
        FirebaseApp.initializeApp(this);
        mFirebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseStorage = FirebaseStorage.getInstance();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        initTargetForProfilePictureDownload();
    }

    public FirebaseAuth getFirebaseAuth() {
        return mFirebaseAuth;
    }

    public FirebaseDatabase getFirebaseDatabase() {
        return firebaseDatabase;
    }

    public User getAuthenticatedUser() {
        return authenticatedUser;
    }

    public void setAuthenticatedUser(User authenticatedUser) {
        this.authenticatedUser = authenticatedUser;
    }

    public void loadAuthenticatedUserProfilePicture() {
        profilePicture = Util.getEmptyProfilePicture(this, authenticatedUser);
        if (Util.SHOULD_DOWNLOAD_PICTURES) {
            StorageReference chattingPartnerProfilePictureReference = FirebaseStorage.getInstance().getReference().child("profile/"
                    + mFirebaseAuth.getUid() + ".jpg");
            chattingPartnerProfilePictureReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.get().load(uri).into(targetForProfilePictureDownload);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.v(TAG, "Profile picture not found");
                    EventBus.getDefault().postSticky(new ProfilePictureDownloadFinishEvent());
                }
            });
        }
    }

    private void initTargetForProfilePictureDownload() {
        targetForProfilePictureDownload = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                profilePicture = bitmap;
                EventBus.getDefault().postSticky(new ProfilePictureDownloadFinishEvent());
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                Log.v(TAG, "Profile picture failed to load");
                EventBus.getDefault().postSticky(new ProfilePictureDownloadFinishEvent());
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                Log.v(TAG, "OnPrepareLoad");
            }
        };
    }

    public Bitmap getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(Bitmap profilePicture) {
        this.profilePicture = profilePicture;
    }
    public static class ProfilePictureDownloadFinishEvent {}

}
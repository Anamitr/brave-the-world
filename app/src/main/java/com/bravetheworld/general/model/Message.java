package com.bravetheworld.general.model;

/**
 * Created by Konrad Sochacki on 10.06.2018.
 */
public class Message {

    private String text;
    private String userId;

    public Message() {
    }

    public Message(String text, String userId) {
        this.text = text;
        this.userId = userId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
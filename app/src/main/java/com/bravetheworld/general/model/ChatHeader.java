package com.bravetheworld.general.model;

/**
 * Created by Konrad Sochacki on 11.01.2019.
 */
public class ChatHeader {
    public String id;
    public String lonelyOne;
    public String joyfulOne;

    public ChatHeader () {}

    public ChatHeader(String id, String lonelyOne, String joyfulOne) {
        this.id = id;
        this.lonelyOne = lonelyOne;
        this.joyfulOne = joyfulOne;
    }
}

package com.bravetheworld.general.model;

/**
 * Created by Konrad Sochacki on 27.01.2018.
 */

public enum RoleEnum {
    LONELY_ONE("Lonely one"), JOYFUL_ONE("Joyful one");

    private final String text;

    private RoleEnum(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
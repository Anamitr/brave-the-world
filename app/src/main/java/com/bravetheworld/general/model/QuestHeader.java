package com.bravetheworld.general.model;

/**
 * Created by Konrad Sochacki on 30.11.2018.
 */
public class QuestHeader {
    String title;
    Integer points;

    public QuestHeader() {
    }

    public QuestHeader(String title, Integer points) {
        this.title = title;
        this.points = points;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }
}

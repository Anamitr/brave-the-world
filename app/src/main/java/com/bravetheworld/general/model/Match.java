package com.bravetheworld.general.model;

/**
 * Created by Konrad Sochacki on 09.06.2018.
 */
public class Match {
    public String id;
    public String lonelyOneId;
    public String joyfulOneId;
    public Integer score;

    public Match () {}

    public Match(String id, String lonelyOneId, String joyfulOneId, Integer score) {
        this.id = id;
        this.lonelyOneId = lonelyOneId;
        this.joyfulOneId = joyfulOneId;
        this.score = score;
    }

    @Override
    public String toString() {
        return "Match{" +
                "id='" + id + '\'' +
                ", lonelyOneId='" + lonelyOneId + '\'' +
                ", joyfulOneId='" + joyfulOneId + '\'' +
                ", score=" + score +
                '}';
    }
}

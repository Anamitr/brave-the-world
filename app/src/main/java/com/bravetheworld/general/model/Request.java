package com.bravetheworld.general.model;

/**
 * Created by Konrad Sochacki on 31.03.2018.
 */
public class Request {
    // Request can be send as yes - then will appear in destined user world and disappear at sender's world
    // or no - then will just disappear
    public boolean choice;
    public String fromUserId;
    public String toUserId;

    public Request() {}

    public Request(boolean choice, String fromUserId, String toUserId) {
        this.choice = choice;
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
    }

    @Override
    public String toString() {
        return "Request{" +
                "choice=" + choice +
                ", fromUserId='" + fromUserId + '\'' +
                ", toUserId='" + toUserId + '\'' +
                '}';
    }
}

package com.bravetheworld.general.model;

import java.io.Serializable;

/**
 * Created by Konrad Sochacki on 16.11.2018.
 */
public class Quest implements Serializable {
    public enum QuestStateEnum {
        CREATED, COMPLETED, CANCELLED
    }

    String id;
    String title;
    String description;
    Integer points;
    QuestStateEnum state = QuestStateEnum.CREATED;

    public Quest() {
    }

    public Quest(String id, String title, String description, Integer points) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.points = points;
    }

    public Quest(String id, String title, String description, Integer points, QuestStateEnum state) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.points = points;
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public QuestStateEnum getState() {
        return state;
    }

    public void setState(QuestStateEnum state) {
        this.state = state;
    }
}

package com.bravetheworld.general.model;

import java.io.Serializable;

/**
 * Created by Konrad Sochacki on 27.01.2018.
 */

public class User implements Serializable{
    public String id;
    public String email;
    public String nick;
    public String age;
    public String city;
    public String role;
    public String deviceToken;

    public User() {
    }

    public User(String id, String email, String nick, String age, String city, String role) {
        this.id = id;
        this.email = email;
        this.nick = nick;
        this.age = age;
        this.city = city;
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }
}